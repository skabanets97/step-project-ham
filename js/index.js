// tabs, section "Our Services"

const tabsServices = document.querySelector('.tabs-list');
const tabsServicesTitle = document.querySelectorAll('.tabs-title');
const tabsServicesContent = document.querySelectorAll('.tabs-content-item');


const setDataAtr = (titles, elementsContent) => {
    titles.forEach(item => {
        item.setAttribute('data-tabs', `${item.innerText}`);
    });

    elementsContent.forEach( (item, index) => {
        item.setAttribute('data-tabs', `${titles[index].getAttribute('data-tabs')}`);
    });
};

setDataAtr(tabsServicesTitle, tabsServicesContent);

tabsServices.addEventListener('click', event => {
    if (event.target !== tabsServices){
            tabsServicesTitle.forEach( item => {
            item.classList.remove('active-title');
        });

        tabsServicesContent.forEach( contentTab => {
            if (contentTab.dataset.tabs === event.target.dataset.tabs) {
                contentTab.classList.add('active-content');
            } else {
                contentTab.classList.remove('active-content');
            }
        });
    }

    event.target.classList.toggle('active-title');
});




