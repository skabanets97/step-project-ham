// section "Our Amazing Work"

var countLoad = 0;

$(".item-wrapper").slice(12, 36).hide();

$("#load-btn").on("click", () => {
    countLoad ++;
    if(countLoad === 1) {
        $(".item-wrapper").slice(0, 24).show().fadeIn(100);
    }
    if (countLoad >= 2) {
        $(".item-wrapper").slice(0, 36).show().fadeIn(100);
        $(".load-btn").hide();
    }
});

$(".filter-element").on("click", function () {

    $(".filter-element").removeClass("active-filter");
    $(this).addClass("active-filter");

    if($("#all").hasClass("active-filter")){
        $(".load-btn").hide();
        $(".item-wrapper").show().fadeIn(100);
    }

    if($("#graphic-design").hasClass("active-filter")){
        $(".item-wrapper").hide();
        $(".load-btn").hide();
        $(".graphic-design").show().fadeIn(100);
    }

    if($("#web-design").hasClass("active-filter")){
        $(".item-wrapper").hide();
        $(".load-btn").hide();
        $(".web-design").show().fadeIn(100);
    }

    if($("#lending-page").hasClass("active-filter")){
        $(".item-wrapper").hide();
        $(".load-btn").hide();
        $(".landing-page").show().fadeIn(100);
    }

    if($("#wordpress").hasClass("active-filter")){
        $(".item-wrapper").hide();
        $(".load-btn").hide();
        $(".wordpress").show().fadeIn(100);
    }
});

// section "What People Say About theHam"

var counter = 0;

const showAvatars = () => {

    if (counter === 0) {
        $(".slider-avatar").show().fadeIn(100);;
        $("#kevin").hide();
        $("#sara").hide();
    }

    if (counter === 1) {
        $(".slider-avatar").show().fadeIn(100);;
        $("#kate").hide();
        $("#sara").hide();
    }

    if (counter === 2) {
        $(".slider-avatar").show().fadeIn(100);;
        $("#kate").hide();
        $("#adam").hide();
    }

};

showAvatars(counter);

$("#arrow-left").addClass("empty");

$(".arrow.right").on("click", () => {
    counter++;
    $(".arrow.left").removeClass("empty");

    if (counter >= 2) {
        $(".arrow.right").addClass("empty");
        counter = 2;
    } else{
        $(".arrow.right").removeClass("empty");
    }
    showAvatars(counter);
});

$(".arrow.left").on("click", () => {
    counter--;
    $(".arrow.right").removeClass("empty");

    if (counter <= 0) {
        $(".arrow.left").addClass("empty");
        counter = 0;
    } else{
        $(".arrow.left").removeClass("empty");
    }
    showAvatars(counter);
});

$(".customer-review").hide();
$(".kate").show().fadeIn(100);

$(".slider-avatar").on("click", function () {
    $(".slider-avatar").removeClass("focus");
    $(this).addClass("focus");

    if($("#kate").hasClass("focus")){
        $(".customer-review").hide();
        $(".kate").show().fadeIn(100);
    }

    if($("#adam").hasClass("focus")){
        $(".customer-review").hide();
        $(".adam").show().fadeIn(100);
    }

    if($("#ali").hasClass("focus")){
        $(".customer-review").hide();
        $(".ali").show().fadeIn(100);
    }

    if($("#megan").hasClass("focus")){
        $(".customer-review").hide();
        $(".megan").show().fadeIn(100);
    }

    if($("#kevin").hasClass("focus")){
        $(".customer-review").hide();
        $(".kevin").show().fadeIn(100);
    }

    if($("#sara").hasClass("focus")){
        $(".customer-review").hide();
        $(".sara").show().fadeIn(100);
    }

});


